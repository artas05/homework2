function addTokens(input, tokens) {
  if (typeof input == "string") {
    if (input.length > 5) {
      for (let i = 0; i < tokens.length; i++) {
        if (
          tokens[i].tokenName == null ||
          typeof tokens[i].tokenName != "string"
        ) {
          throw new Error("Invalid array format");
        }
      }

      let i = 0;
      while (input.indexOf("...") != -1) {
        input = input.replace("...", "${" + tokens[i].tokenName + "}");
        i++;
      }

      return input;
    } else {
      throw new Error("Input should have at least 6 characters");
    }
  } else {
    throw new Error("Invalid input");
  }
}

const app = {
  addTokens: addTokens
};

module.exports = app;
